﻿using System.Data.Entity;

using QC.ORM.Models;

namespace QC.ORM.Migrations
{
    public class AirportMigration : DbContext
    {
        public virtual DbSet<AirportModel> Airports { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AirportModel>();
        }
    }
}
