﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QC.ORM.Models
{
    [Table("Airports")]
    public class AirportModel
    {
        [Required]
        public int Id { get; set; }

        [StringLength(10)]
        public string Iata { get; set; }

        [StringLength(50)]
        public string Lon { get; set; }

        [StringLength(10)]
        public string Iso { get; set; }

        public Nullable<Byte> Status { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(10)]
        public string Continent { get; set; }

        [StringLength(50)]
        public string Type { get; set; }

        [StringLength(50)]
        public string Lat { get; set; }

        [StringLength(50)]
        public string Size { get; set; }
    }
}
