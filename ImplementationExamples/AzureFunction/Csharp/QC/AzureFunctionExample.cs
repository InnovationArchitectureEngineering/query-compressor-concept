/**
 * Query Compression
 * Implementation example in C# Azure Function (Serverless)
 * Version: 2.5.0
 * 
 * Features:
 * - Smart DataProcessor, QueryBuilder & QueryCompressor
 * -- Benefit: Adaptive to variable input data schema & converts from NoSQL to RDBMS
 * -- Benefit: Maximum economy at "Queries sent to DB", for High Traffic systems
 * 
 * Description:
 * - Event: Gets triggered by HTTP POST Request
 * - Input: Reads raw NoSQL json data from URI
 * - Result: Aggregates & filters NoSQL data & stores to RDBMS compressed to 1 query per 1000 records
 * 
 * Dependencies: Microsoft.Net.Sdk.Functions, EntityFramework, System.Web.Extentions
 * 
 * (c) https://www.linkedin.com/in/arissc/
 * License: MIT
 */

using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;

using QC.ORM.Models;
using QC.ORM.Migrations;

namespace QC
{
    public static class AzureFunctionExample
    {
        // Constants
        // This implementation example is using the following URL to get demo data for testing purpose
        private static readonly string filePath = "https://raw.githubusercontent.com/jbrooksuk/JSON-Airports/master/airports.json";
        private static readonly string queryValueFilter = "EU"; // Of course this filter can be passed dynamically as argument via REST
        //private static readonly string storageSet = "[(localdb)].[dbo].[Airports]"; // Send query to localdb for testing
        private static readonly string storageSet = "MY AWS RDS Instance"; // Send query to AWS RDS
        private static readonly string queryStrSet = "INSERT INTO " + storageSet + " (Iata, Lon, Iso, Status, Name, Continent, Type, Lat, Size) VALUES ";

        // Azure Function
        [FunctionName("QC")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            // Report Msg Log
            log.Info("Database update request has been received");

            HttpResponseMessage response;

            try
            {
                // JSON Parse
                string stringJson = null;
                HttpClient client = new HttpClient();
                HttpResponseMessage responseData = await client.GetAsync(filePath);
                responseData.EnsureSuccessStatusCode();
                stringJson = await responseData.Content.ReadAsStringAsync();

                if (stringJson != null) {

                    // Deserialize JSON data
                    JavaScriptSerializer s = new JavaScriptSerializer();

                    var airports = s.Deserialize<List<HelperModel>>(stringJson);

                    // Report Msg Log
                    log.Info("Data has been successfully received from the HTTP feed - Saving data to database has been initiated");

                    /* Smart DataProcessor, QueryBuilder & QueryCompressor - START */

                    using (var db = new AirportMigration()) {

                        // Instanciate object to execute DB queries
                        var objContxt = ((IObjectContextAdapter)db).ObjectContext;

                        // Clocks & Calculations Vars
                        int queryLimit = 1000;
                        int queryLimitClock = 0;
                        int queryLimitCounter = 0;
                        int queryLimitDivision = 0;
                        int queryLimitRemainder = 0;
                        int queryLimitRemainderClock = 0;

                        // Count the total number of aggregated records by given filter
                        foreach (var airport in airports) {
                            if (airport.Continent == queryValueFilter) {
                                queryLimitCounter++;
                            }
                        }
                        
                        // Useful calculations
                        queryLimitDivision = queryLimitCounter / queryLimit;
                        queryLimitRemainder = queryLimitCounter % queryLimit;

                        // Debugging Logs
                        // log.Info("queryLimitCounter = " + queryLimitCounter);
                        // log.Info("queryLimitDivision = " + queryLimitDivision);
                        // log.Info("queryLimitRemainder = " + queryLimitRemainder);

                        // In case aggregation did not found requested data
                        if (queryLimitCounter < 1) { log.Info("No records with European Airports were found in HTTP feed");  }

                        // In case aggregation found requested data
                        else {

                            // Report Msg Log
                            log.Info("Records of European Airports have been found. Total number of EU airports: " + queryLimitCounter);

                            // MS SQL server - query Identifier length limit FIX
                            // possibly also fixes concatenation in queries execution as well: "string" + stringVar
                            objContxt.ExecuteStoreCommand("SET QUOTED_IDENTIFIER OFF");

                            // Truncate DB table before store updated data
                            objContxt.ExecuteStoreCommand("TRUNCATE TABLE " + storageSet);

                            // Report Msg Log
                            log.Info("DB table has been cleared from older data");

                            
                            // Query building helper vars
                            string queryStr = queryStrSet;
                            string queryRow = "";
                            int queryRowCounter = 0;
                            char[] filterChar = { ',' };
                            string queryStrFiltered = "";

                            // Smart Query Builder

                            foreach (var airport in airports) {

                                if (airport.Continent == queryValueFilter) {

                                    queryRowCounter++;

                                    queryRow += "(";

                                    // Smart Data Processor - Query Building Batch
                                    // with protection of DB Server against harmful unescaped chars

                                    if (airport.Iata != null) { queryRow += "\'" + WebUtility.HtmlEncode(airport.Iata) + "\', "; }
                                    else { queryRow += "null, "; }

                                    if (airport.Lon != null) { queryRow += "\'" + WebUtility.HtmlEncode(airport.Lon) + "\', "; }
                                    else { queryRow += "null, "; }

                                    if (airport.Iso != null) { queryRow += "\'" + WebUtility.HtmlEncode(airport.Iso) + "\', "; }
                                    else { queryRow += "null, "; }

                                    // Extra Protection of C# compiler against "NoSQL input JSON: int->nulls" (in DB Query this value will be sent as integer ;)
                                    if (airport.Status.ToString() != null) { queryRow += WebUtility.HtmlEncode(airport.Status.ToString()) + ", "; }
                                    else { queryRow += "null, "; }

                                    if (airport.Name != null) { queryRow += "\'" + WebUtility.HtmlEncode(airport.Name) + "\', "; }
                                    else { queryRow += "null, "; }

                                    if (airport.Continent != null) { queryRow += "\'" + WebUtility.HtmlEncode(airport.Continent) + "\', "; }
                                    else { queryRow += "null, "; }

                                    if (airport.Type != null) { queryRow += "\'" + WebUtility.HtmlEncode(airport.Type) + "\', "; }
                                    else { queryRow += "null, "; }

                                    if (airport.Lat != null) { queryRow += "\'" + WebUtility.HtmlEncode(airport.Lat) + "\', "; }
                                    else { queryRow += "null, "; }

                                    // last in the batch (without ", ")
                                    if (airport.Size != null) { queryRow += "\'" + WebUtility.HtmlEncode(airport.Size) + "\'"; }
                                    else { queryRow += "null"; }
                                    
                                    queryRow += "),"; // "space" char skipped to simplify final filtering
                                    queryStr += queryRow;
                                    queryRow = ""; // possibly unecessary line, but more secure coding approach

                                } // if END
                                
                                // Smart Query Compressor

                                if (queryLimitDivision > 0) {

                                    if ((queryLimitClock < queryLimitDivision) && (queryRowCounter == queryLimit)) {

                                        // Debugging Log    
                                        // log.Info("Entered queryRowCounter == queryLimit");

                                        queryLimitClock++;

                                        // Filter last "," from query string
                                        queryStrFiltered = queryStr.TrimEnd(filterChar);

                                        // Query execution
                                        objContxt.ExecuteStoreCommand(queryStrFiltered);

                                        // Restore helper vars to defaults
                                        queryStr = queryStrSet;
                                        queryRowCounter = 0;
                                        queryStrFiltered = "";
                                    }

                                    else {

                                        if ((queryLimitRemainderClock < 1) && (queryRowCounter == queryLimitRemainder)) {

                                                queryLimitRemainderClock++;

                                                // Debugging Logs
                                                // log.Info("Entered queryRowCounter == queryLimitRemainder");
                                                // log.Info("queryRowCounter = " + queryRowCounter);

                                                queryStrFiltered = queryStr.TrimEnd(filterChar);

                                                objContxt.ExecuteStoreCommand(queryStrFiltered);

                                        } // if END

                                    } // else END

                                } // if END

                                else {

                                    if ((queryLimitRemainderClock < 1) && (queryRowCounter == queryLimitRemainder)) {

                                            queryLimitRemainderClock++;

                                            queryStrFiltered = queryStr.TrimEnd(filterChar);

                                            objContxt.ExecuteStoreCommand(queryStrFiltered);

                                    } // if END

                                } // else END

                            } // foreach END

                            objContxt.ExecuteStoreCommand("SET QUOTED_IDENTIFIER ON");

                        } // else END

                    } // using END

                    // Report Msg Log
                    log.Info("Smart DataProcessor, QueryBuilder & QueryCompressor tasks have been completed successfully");
                    
                    /* Smart DataProcessor, QueryBuilder & QueryCompressor - END */

                    var successMessage = "Azure Function has succeeded";
                    response = req.CreateResponse(HttpStatusCode.OK, successMessage);

                } // if END

                else {
                    var errorMessage = "Azure Function has failed";
                    log.Error(errorMessage);
                    response = req.CreateErrorResponse(HttpStatusCode.BadRequest, errorMessage);
                }

            } // try END

            catch (Exception e) {
                log.Error(e.Message, e);
                response = req.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }

            return response;
        }
    }
}
